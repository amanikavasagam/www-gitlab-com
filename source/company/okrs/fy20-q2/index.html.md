---
layout: markdown_page
title: "FY20-Q2 OKRs"
---

This quarter will run from May 1, 2019 to July 31, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. Grow website visitors. Introduce a certification program. Add a DevOps Transformation offering.

- CRO: 
- CFO:
- CPO:
- CMO:
- VP Product:
- VP Eng:
- VP Alliances:

### CEO: Popular next generation product. Launch four growth groups. Deliver on maturity and new categories. Add consumption pricing for compute.

- CRO: 
- CFO:
- CPO:
- CMO:
- VP Product:
- VP Eng:
- VP Alliances:

### CEO: Great team. Become known as the company for all remote. Get all KPIs and OKRs from dashboards instead of slides. Handbook more accessible and SSoT.

- CRO: 
- CFO:
- CPO:
- CMO:
- VP Product:
- VP Eng:
- VP Alliances:
