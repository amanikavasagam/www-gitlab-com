---
layout: markdown_page
title: "Category Vision - Code Review"
---

- TOC
  {:toc}

## Code Review

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Review code, discuss changes, share knowledge, and identify defects in code among distributed teams via asynchronous review and commenting. Automate, track and report code reviews.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=code%20review)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=code%20review)
- [Overall Vision](/direction/create/)

Please reach out to PM James Ramsay ([E-Mail](jramsay@gitlab.com)
[Twitter](https://twitter.com/jamesramsay)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

Code review is used heavily by software engineers, or any other kind of individual contributor that commits changes to Git repositories. Depending on the context, however, their workflow and experience of code review may be quite different.

- **full time contributor** to a commercial product where reducing cycle time is very important. The review cycle is very tight and focussed as a consequence of best practices where keeping merge requests small and iterating at a high velocity are objectives. Code review workflows for these users are **Complete**
- **occasional contributor** to an open source product where cycle time is typically longer as a consequence that they are not working on the project full time. This results in longer review times. When long review times occur, the participants in the merge request will need to spend more time reacquainting themselves with the change. When there are non-trivial amounts of feedback this can be difficult to understand. Code review workflows for these users are **Complete**
- **scientific projects** frequently have a different flow to typical projects, where the development is sporadic, and changes are often reviewed after they have been merged to master. This is a consequence of the high code churn associated with high exploratory work, and having infrequent access to potential reviewers. Post-merge code review workflows are not yet viable in GitLab.

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

- **In progress (11.10): [Multiple assignees](https://gitlab.com/gitlab-org/gitlab-ee/issues/2004)** - Merge requests frequently involve collaboration between multiple developers, however a merge request can only be assigned to one person. Like issues, merge requests should support multiple assignees.
- **Next (ETA 12.0): [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854)** - Code reviews are expensive, requiring engineers to carefully review the merge request diff to understand the changes being proposed. The accuracy of the diff is critical for effective code reviews. Additionally, both Atlassian and GitHub have made their diffs smarter, showing the actual difference between the source and target branch, not the source branch and the merge base of the target branch.
- **Next: [Re-review and approval for merge requests](https://gitlab.com/groups/gitlab-org/-/epics/314)** - After a code review, the author will address the feedback with various changes and clarifying questions which the reviewer needs to answer and review before approving. This isn't a lack of trust, but a matter of quality and mentorship so that the feedback is addressed in full and correctly. This _post-review pre-approval workflow_ is currently very difficult. We need to make this better.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

GitLab competes with both integrated and dedicated code review tools. Because merge requests (which is the code review interface), and more specifically the merge widget, is the single source of truth about a code change and a critical control point in the GitLab workflow, it is important that merge requests and code review in GitLab is excellent. Our primary source of competition and comparison is to dedicated code review tools.

Prospects and new customers, who previously used dedicated code review tools typically have high expectations and accustomed to a high degree of product depth. Given that developers spend a significant portion (majority?) of their in application time in merge requests, limitations are quickly noticed and become a source of frustration.

Integrated code review packaged with source code management:

- [GitHub](https://github.com/features/code-review/)
- [Bitbucket](https://bitbucket.org/product/features) by Atlassian

Dedicated code review tools:

- [Crucible](https://www.atlassian.com/software/crucible) by Atlassian ([Bitbucket vs Crucible](https://confluence.atlassian.com/bitbucketserverkb/what-s-the-difference-between-crucible-and-bitbucket-server-do-i-need-both-779171640.html))
- [Phabricator](https://www.phacility.com/phabricator/) by Phacility ([example](https://phabricator.haskell.org/D4953))
- [Review Board](https://www.reviewboard.org/) ([example](http://demo.reviewboard.org/r/844/diff/1/#index_header))
- [Reviewable](https://reviewable.io/) ([example](https://reviewable.io/reviews/Reviewable/demo/1))

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Per branch merge request approval rules](https://gitlab.com/gitlab-org/gitlab-ee/issues/460)
- [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854)
- [Cross-project code review (group merge requests)](https://gitlab.com/groups/gitlab-org/-/epics/457)

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Per branch merge request approval rules](https://gitlab.com/gitlab-org/gitlab-ee/issues/460)
- [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854)

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- [Re-review and approval](https://gitlab.com/groups/gitlab-org/-/epics/314) has been raised a number of times internally, that as merge requests age and more reviews are performed, it becomes increasingly difficult to work out what is going on, both as a developer and reviewer/approver

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Re-review and approval](https://gitlab.com/groups/gitlab-org/-/epics/314)
- [Commit messages as part of code review](https://gitlab.com/groups/gitlab-org/-/epics/286)
- [Code review any line on any file in a merge request](https://gitlab.com/groups/gitlab-org/-/epics/19)
- [Commit by commit code review](https://gitlab.com/groups/gitlab-org/-/epics/285)

