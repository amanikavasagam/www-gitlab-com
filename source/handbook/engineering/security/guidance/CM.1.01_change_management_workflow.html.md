---
layout: markdown_page
title: "CM.1.01 - Change Management Workflow Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# CM.1.01 - Change Management Workflow

## Control Statement

Change scope, change type, and roles and responsibilities are pre-established and documented in a change control workflow; notification and approval requirements are also pre-established based on risk associated with change scope and type.

## Context

Having a structured workflow and guidance on change management helps reduce the risk of GitLab experiencing platform or application instability by increasing the predictability and reproducibility of the change management process.

## Scope

This control applies to the GitLab.com production environment.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CM.1.01_change_management_workflow.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CM.1.01_change_management_workflow.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CM.1.01_change_management_workflow.md).

## Framework Mapping

* ISO
  * A.12.1.2
  * A.12.6.2
  * A.14.2.1
  * A.14.2.2
  * A.14.2.4
* SOC2 CC
  * CC2.3
  * CC8.1
* PCI
  * 1.1.1
  * 10.4.2
  * 6.4
  * 6.4.5
  * 6.4.5.1
  * 6.4.5.2
  * 6.4.5.3
  * 6.4.5.4
  * 6.4.6
